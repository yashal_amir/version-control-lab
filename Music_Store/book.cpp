#include "product.h"
#include "book.h"
#include <iostream>

/*
book.cpp
Author: M00696562
Created: 21/01/2021
Updated: 22/01/2021
*/
using namespace std;

Book::Book(int t_pCode , string t_pName, int t_price, string t_author, string t_isbn, string t_genre, int t_year, string t_state) : Product(t_pCode, t_pName, t_price, t_state) {
    author = t_author;
    isbn = t_isbn;
    genre = t_genre;
    year = t_year;
}

string Book::getInfo() {
	char pCodeStr[100], priceStr[100], yearStr[100];
	sprintf ( pCodeStr, "%d", pCode);
	sprintf ( priceStr, "%d", price);
	sprintf ( yearStr, "%d", year);
	string result  = "Book ";
	result += pCodeStr;
	result +=  " " + pName + " " + priceStr + " " + author + " " + isbn + " " + genre + " " + yearStr + " " + state;
	return result;
}

Book* Book::dupProduct() {
	Book *newBook = new Book ( pCode , pName, price, author, isbn, genre, year, state);
	return newBook;
}
